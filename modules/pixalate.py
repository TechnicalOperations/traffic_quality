import requests
import re
import datetime
import json

class Pixalate:

	# Create session with server
	def login(self):
		login_url = "https://idp.pixalate.com/services/2001/Realm/login?username=ro&password=RadRhythm1!&rememberMe=false&callback=_jqjsp"
		headers = {
			'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
		}

		r = requests.session()
		try:
			req = r.get(login_url, headers=headers)
			if req.status_code == 200:
				return r
			else:
				return -1
		except Exception as e:
			print(e)
			return -1

	# Query predefined reporting
	def query_report(self,r):
		# Get yesterday timedate
		def get_date():
			cur_date = datetime.datetime.today()
			day = datetime.timedelta(days=1)
			yest = cur_date - day
			yest = str(yest).split()[0]
			return yest

		yest = get_date()
		q_url = "https://dashboard-srvc.pixalate.com/services/2016/Report/getDetails?reportId=overview&timeZone=0&q=publisherId%2CadvertiserName%2Ckv7%2CsellerName%2CsiteId%2CsiteName%2Cimpressions%2CsivtImpressions%2CsivtImpsRate%2CgivtImpressions%2CgivtImpsRate%20WHERE%20day%20%3D%20%27{}%27%20AND%20kv7%20%3D%20%272058%27%20ORDER%20BY%20impressions%20DESC&start=0&limit=300&type=undefined&callback=_jqjsp".format(yest)
		headers = {
			'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
		}

		try:
			req = r.get(q_url, headers=headers)
			if req.status_code == 200:
				m = re.search('_jqjsp\((.+)\);', req.text)
				if m:
					try:
						q_results = json.loads(m.group(1))
						return q_results['docs']
					except Exception as e:
						print(e)
						return -1
			else:
				print("Failed")
		except Exception as e:
			print(e)
			return -1