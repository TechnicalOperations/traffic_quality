import snowflake.connector
import datetime


class SnowQuery:

	# Get date range
	def date_builder(self):
		cur_date = datetime.datetime.today()
		start_date = str(cur_date).split()[0]
		day = datetime.timedelta(days=7)
		end_date = cur_date - day
		end_date = str(end_date).split()[0]
		return [start_date, end_date]


	# SQL query builder
	def sql_builder(self,db):
		# Snowflake login info
		ACCOUNT = 'rhythmone'
		WAREHOUSE = 'QUERY_WH'
		if db == "edw":
			DATABASE = 'R1_EDW'
		if db == "ods":
			DATABASE = 'ODS'
		USER = '<USER>'
		PASS = '<PASSWORD>'

		# Attempt login
		try:
			cnx = snowflake.connector.connect(
				user=USER,
				password=PASS,
				account=ACCOUNT)
		except Exception as e:
			print(e)
			return -1

		# Cursor object
		cur = cnx.cursor()

		# Setup warehouse and database to use
		cur.execute("USE warehouse {}".format(WAREHOUSE))
		cur.execute("USE {}".format(DATABASE))
		return cur


	#SQL query for AK impressions
	def ak_imp(self):

		# Get date range
		start_date, end_date = self.date_builder()

		# Store queried results
		a = []

		# SQL query
		cur = self.sql_builder("edw")
		try:
			# AK Impressions
			cur.execute("SELECT ssp_id, pub_id, site_id, SUM(IMPRESSION_PIXEL), SUM(COST)" +
				" FROM r1_edw.rx.impression_d_est" +
				" WHERE event_date <= '{}'".format(start_date) +
				" AND event_date >= '{}'".format(end_date) +
				" AND ssp_id = 'rmpak1'" +
				" GROUP BY 1,2,3")
			for i in cur:
				a.append(i)

		finally:
			# Close cursor
			cur.close()

		# Return response array
		return a


	#SQL query for 1R impressions
	def r1_imp(self):

		# Get date range
		start_date, end_date = self.date_builder()

		# Store queried results
		a = []

		# SQL query
		cur = self.sql_builder("edw")
		try:
			# AK Impressions
			cur.execute("SELECT ssp_id, pub_id, site_id, SUM(IMPRESSION_PIXEL), SUM(COST)" +
				" FROM r1_edw.rx.impression_d_est" +
				" WHERE event_date <= '{}'".format(start_date) +
				" AND event_date >= '{}'".format(end_date) +
				" AND ssp_id IN ('rmphb','rmp1r1','rmpssp')" +
				" GROUP BY 1,2,3")
			for i in cur:
				a.append(i)

		finally:
			# Close cursor
			cur.close()

		# Return response array
		return a


	#SQL query for AK RG Ratings
	def ak_rg(self):

		# Get date range
		start_date, end_date = self.date_builder()

		# Store queried results
		a = []

		# SQL query
		cur = self.sql_builder("ods")
		try:
			# AK Impressions
			cur.execute("SELECT ssp_id, pub_id, site_id, source_rating, IAS_RATE, SUM(sample_rate) AS totalRequests" +
				" FROM ods.rx.bidrequest" +
				" WHERE event_time <= '{}'".format(start_date) +
				" AND event_time >= '{}'".format(end_date) +
				" AND ssp_id = 'rmpak1'" +
				" GROUP BY 1,2,3,4,5")
			for i in cur:
				a.append(i)

		finally:
			# Close cursor
			cur.close()

		# Return response array
		return a


	#SQL query for 1R RG Ratings
	def r1_rg(self):

		# Get date range
		start_date, end_date = self.date_builder()

		# Store queried results
		a = []

		# SQL query
		cur = self.sql_builder("ods")
		try:
			# AK Impressions
			cur.execute("SELECT ssp_id, pub_id, site_id, source_rating, IAS_RATE, DV_RATE, SUM(sample_rate) AS totalRequests" +
				" FROM ods.rx.bidrequest" +
				" WHERE event_time <= '{}'".format(start_date) +
				" AND event_time >= '{}'".format(end_date) +
				" AND ssp_id IN ('rmphb','rmp1r1','rmpssp')" +
				" GROUP BY 1,2,3,4,5,6")
			for i in cur:
				a.append(i)

		finally:
			# Close cursor
			cur.close()

		# Return response array
		return a
