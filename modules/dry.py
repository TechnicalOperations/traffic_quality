from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

import time

class Worker(QRunnable):
	'''
	Worker thread
	'''

	@pyqtSlot()
	def run(self):
		'''
		Your code goes in this function
		'''
		print("Thread start")
		time.sleep(5)
		print("Thread complete")


class MainWindow(QMainWindow):


	def __init__(self, *args, **kwargs):

		super(MainWindow, self).__init__(*args, **kwargs)

		self.counter = 0

		layout = QVBoxLayout()

		self.l = QLabel("Start")
		b = QPushButton("DANGER!")
		b.pressed.connect(self.oh_no)

		layout.addWidget(self.l)
		layout.addWidget(b)

		w = QWidget()
		w.setLayout(layout)

		self.setCentralWidget(w)

		self.show()

		self.threadpool = QThreadPool()
		self.threadpool.setMaxThreadCount(self.threadpool.maxThreadCount() * 2)
		print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())

		self.timer = QTimer()
		self.timer.setInterval(1000)
		self.timer.timeout.connect(self.recurring_timer)
		self.timer.start()

	def oh_no(self):
		worker = Worker()
		self.threadpool.start(worker)

	def done(self):
		print("Done")

	def recurring_timer(self):
		self.counter +=1
		self.l.setText("Counter: %d" % self.counter)


app = QApplication([])
window = MainWindow()
app.exec_()