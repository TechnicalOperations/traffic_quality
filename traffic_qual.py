import queue
import sys
import os.path
import threading

#from openpyxl import Workbook
import xlsxwriter
#from openpyxl.styles import Font, Color, Alignment

# Custom Module Import
sys.path.append(os.path.join(os.path.dirname(__file__),"./modules"))
from pixalate import Pixalate
from snow_query import SnowQuery


#########################
## Functions to thread ##
#########################

# Pixalate information
def pixa(q_pixa):
	print("[+]Executing PIXA queries")
	sql = Pixalate()
	sid = sql.login()
	a = sql.query_report(sid)
	q_pixa.put(a)
	q_pixa.task_done()

# AK zone information
def ak(q_ak):
	print("[+]Executing AK queries")
	sql = SnowQuery()
	resp_imp = sql.ak_imp()
	resp_rg = sql.ak_rg()
	for i in resp_rg:
		if i[2] == None:
			resp_rg.remove(i)

	a = {}
	a['ak_imp'] = resp_imp
	a['ak_rg'] = resp_rg
	q_ak.put(a)
	q_ak.task_done()

# R1 zone information
def r1(q_r1):
	print("[+]Executing 1R queries")
	sql = SnowQuery()
	resp_imp = sql.r1_imp()
	resp_rg = sql.r1_rg()
	a = {}
	a['r1_imp'] = resp_imp
	a['r1_rg'] = resp_rg
	q_r1.put(a)
	q_r1.task_done()

#########################
##  End function list  ##
#########################


# AK Report building function
def ak_excel_write(ret_val_pixa,ret_val_ak):
	# Create Excel Workbook
	print("\t[-]Building Excel doc")
	try:
		wb = xlsxwriter.Workbook('excel/test.xlsx')
		report_ws = wb.add_worksheet('AK SSP Report')
		pixa_ws = wb.add_worksheet('Pixalate')
		ias_ws = wb.add_worksheet('IAS_SourceRating')
		imp_ws = wb.add_worksheet('Impressions')
	except Exception as e:
		print("[!]Error")
		print(e)

	# Build Report Sheet
	def report_sheet(site_ids):
		print("[+]Creating Report Sheet")
		# Recycle date range
		d = SnowQuery()
		start_date,end_date = d.date_builder()


		# Build layout
		bold = wb.add_format({'bold': True})
		italic = wb.add_format({'italic': True})
		report_ws.set_column('A:J', 20)
		report_ws.write('A1', '7 Day Traffic Quality Report', bold)

		report_ws.write('A2', "FROM: {}".format(end_date))
		report_ws.write('A3', "TO: {}".format(start_date))
		report_ws.write('G1', 'Note: Composite score is only used as a way of sorting rows.', italic)

		column_alpha = 'ABCDEFGHI'
		column_n = ['SSP_ID','PUB_ID','SITE_ID','Total Requests',
					'Total Impressions','Pixalate > 500','IAS',
					'SourceRating','Composite Score']
		for l in column_alpha:
			report_ws.write("{0}5".format(l), column_n[column_alpha.index(l)])

		x = 6
		for r in site_ids:
			report_ws.write('A{}'.format(x), '{}'.format(r[0]))
			report_ws.write('B{}'.format(x), '{}'.format(r[1]))
			report_ws.write('C{}'.format(x), '{}'.format(r[2]))
			report_ws.write_array_formula('D{}'.format(x), "=VLOOKUP(C%s, IAS_SourceRating!C:F, 4, 0)" % x)
			report_ws.write_array_formula('E{}'.format(x), "=VLOOKUP(C%s,Impressions!C:E,2,0)" % x)
			report_ws.write_array_formula('F{}'.format(x), "=VLOOKUP(C%s,Pixalate!C:L,10,0)" % x)
			report_ws.write_array_formula('G{}'.format(x), "=IF(VLOOKUP(C%s,IAS_SourceRating!C:E,3,0)=\"\",\"NoData\",VLOOKUP(C%s,IAS_SourceRating!C:E,3,0))" % (x,x))
			report_ws.write_array_formula('H{}'.format(x), "=VLOOKUP(C%s,IAS_SourceRating!C:E,2,0)" % x)
			report_ws.write_array_formula('I{}'.format(x), "=( IF(ISNUMBER(F%s), (F%s*100),0) + IF(ISNUMBER(#REF!),(#REF!*100),0) + IF(ISNUMBER(G%s), G%s*10,0) + IF(ISNUMBER(#REF!),(#REF!*10),0))" % (x,x,x,x))
			x += 1

		'''
		# Build data
		x = 6
		for i in site_ids:
			report_ws["A{}".format(x)] = i[0]  # SSP_ID
			report_ws["B{}".format(x)] = i[1]  # PUB_ID
			report_ws["C{}".format(x)] = i[2]  # SITE_ID
			report_ws["D{}".format(x)] = "=VLOOKUP(C{}, IAS_SourceRating!C:F, 4, 0)".format(x)
			report_ws["E{}".format(x)] = "=VLOOKUP(C{},Impressions!C:E,2,0)".format(x)
			if i[2] not in pixa_siteids:
				report_ws["F{}".format(x)] = "No Data"
			else:
				report_ws["F{}".format(x)] = '=VLOOKUP(C{},Pixalate!C:L,10,0)'.format(x)

			report_ws["G{}".format(x)] = '=IF(VLOOKUP(C{0},IAS_SourceRating!C:E,3,0)="NULL","NoData",VLOOKUP(C{0},IAS_SourceRating!C:E,3,0))'.format(x)
			report_ws["H{}".format(x)] = '=VLOOKUP(C{},IAS_SourceRating!C:E,2,0)'.format(x)
			report_ws["I{}".format(x)] = '=( IF(ISNUMBER(F{0}), (F{0}*100),0) + IF(ISNUMBER(#REF!),(#REF!*100),0) + IF(ISNUMBER(G{0}), G{0}*10,0) + IF(ISNUMBER(#REF!),(#REF!*10),0))'.format(x)
			x += 1
		'''
		return

	# Build Impression Sheet
	def imp_sheet():
		site_ids = []
		column_alpha = 'ABCDE'
		header = ["SSP_ID","PUB_ID","SITE_ID","IMPRESSIONS","COST"]

		for l in column_alpha:
			imp_ws.write('{}1'.format(l),header[column_alpha.index(l)])

		x = 2
		for i in ret_val_ak['ak_imp']:
			for l in column_alpha:
				imp_ws.write("{0}{1}".format(l,x), i[column_alpha.index(l)])
			x += 1
			tmp_arr = [i[0],i[1],i[2]]
			site_ids.append(tmp_arr)
		return site_ids

	# Build IAS Sheet
	def ias_sheet():
		column_alpha = 'ABCDEF'
		header = ["SSP_ID","PUB_ID","SITE_ID","SOURCE_RATING","IAS_RATE","TOTALREQUESTS"]

		for l in column_alpha:
			ias_ws.write('{}1'.format(l),header[column_alpha.index(l)])

		x = 2
		for i in ret_val_ak['ak_rg']:
			ias_ws.write('A{}'.format(x), i[0])
			ias_ws.write('B{}'.format(x), i[1])
			ias_ws.write('C{}'.format(x), i[2])
			ias_ws.write('D{}'.format(x), i[3])
			ias_ws.write('E{}'.format(x), i[4])
			ias_ws.write('F{}'.format(x), i[5])

			x += 1
		return

	# Build Pixalate Sheet
	def pixa_sheet():
		tmp_arr = []
		column_alpha = 'ABCDEFGHIJKL'
		header = ["PUB_ID", "PUB_NAME", "SITE_ID", "SITE_NAME",
						"SELLER_ID","SELLER_NAME","GROSS_IMPS","SIVT_IMPS","SIVT_%",
						"GIVT_IMPS","GIVT_%","TOTAL > 500"]



		for l in column_alpha:
			pixa_ws.write('{}1'.format(l),header[column_alpha.index(l)])

		x = 2
		for i in ret_val_pixa:
			pixa_ws.write('A{}'.format(x), i['publisherId'])
			pixa_ws.write('B{}'.format(x), i['advertiserName'])
			pixa_ws.write('C{}'.format(x), i['siteId'])
			pixa_ws.write('D{}'.format(x), i['siteName'])
			pixa_ws.write('E{}'.format(x), i['kv7'])
			pixa_ws.write('F{}'.format(x), i['sellerName'])
			pixa_ws.write_number('G{}'.format(x), i['impressions'])
			pixa_ws.write_number('H{}'.format(x), i['sivtImpressions'])
			pixa_ws.write_number('I{}'.format(x), round(i['sivtImpsRate'],3))
			pixa_ws.write_number('J{}'.format(x), i['givtImpressions'])
			pixa_ws.write_number('K{}'.format(x), round(i['givtImpsRate'],3))
			pixa_ws.write_formula('L{}'.format(x), "{=IF(G%s<500,\"Pass\",SUM(I%s,K%s))}" % (x,x,x))

			x += 1

			tmp_arr.append(i['siteId'])
		return tmp_arr

	# Output report to file
	def wb_save():
		print("\t[-]Saving document: test.xlsx")
		try:
			wb.save("excel/test.xlsx")
			print("\t[-]Save complete")
		except Exception as e:
			print(e)

	# Sub-function calls
	pixa_siteids = pixa_sheet()
	ias_sheet()
	site_ids = imp_sheet()
	report_sheet(site_ids)


	print("[+]Saving/Closing Workbook")
	try:
		wb.close()
		print("\t[-]Save Complete")
	except Exception as e:
		print(e)

	return



# Main thread
def main():
	# Build threadpool (asynchornous)
	q_pixa = queue.Queue()
	q_ak = queue.Queue()
	q_r1 = queue.Queue()

	# Threaded function calls
	thr_pixa = threading.Thread(target=pixa, args=(q_pixa,))
	thr_ak = threading.Thread(target=ak, args=(q_ak,))
	thr_r1 = threading.Thread(target=r1, args=(q_r1,))
	thrs = [thr_pixa, thr_ak]

	# Start threads
	for i in thrs:
		i.start()

	# Get return values from threads
	ret_val_pixa = q_pixa.get()
	ret_val_ak = q_ak.get()
	#ret_val_r1 = q_r1.get()

	# Join queues
	q_pixa.join()
	q_ak.join()
	#q_r1.join()

	# Join threads
	for i in thrs:
		print("\t[-]Joining: {}".format(i))
		i.join()

	# Build AK Report
	try:
		print("[+]Sending data to Excel Builder")
		ak_excel_write(ret_val_pixa,ret_val_ak)
		print("[+]Returning to Main")

	except Exception as e:
		print(e)
	return



# Entry point
if __name__ == '__main__':
	main()
	sys.exit(0)
